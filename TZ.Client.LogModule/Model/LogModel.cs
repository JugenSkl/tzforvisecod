﻿using Prism.Mvvm;
using System;

namespace TZ.Client.LogModule.Model
{
    class LogModel: BindableBase
    {
        private DateTime _dateTime;
        public DateTime DateTime
        {
            get { return _dateTime; }
            set { SetProperty(ref _dateTime, value); }
        }

        private string _messege;
        public string Messege
        {
            get { return _messege; }
            set { SetProperty(ref _messege, value); }
        }
    }
}
