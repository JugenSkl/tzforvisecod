﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TZ.Client.Base
{
    public static class RegionNames
    {
        public const string WorkingRegion = "WorkingRegion";
        public const string RightRegion = "RightRegion";
        public const string BottomRegion = "BottomRegion";
    }
}
