﻿using System.Globalization;
using System.Text.RegularExpressions;
using System.Windows.Controls;

namespace TZ.Client.FBAdminModule.Rules
{
    public class PhoneNumberRule : ValidationRule
    {
        string pattern = @"^((\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$";

        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            string phoneNumber = value as string;
            if (Regex.IsMatch(phoneNumber, pattern))
            {
                return new ValidationResult(true, null);
            }
            else
            {
                return new ValidationResult(false, "Phone number is incorrect. The number should start +7.");
            }
        }
    }
}
