﻿using Prism.Mvvm;
using Prism.Commands;
using Prism.Events;
using TZ.Client.FBAdminModule.Models;
using TZ.Client.FBAdminModule.Classes;
using TZ.Client.Base.Events;
using System.ComponentModel;

namespace TZ.Client.FBAdminModule.ViewModels
{
    class AddNewUserViewModel: BindableBase
    {
        public AddNewUserViewModel(EventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
            CreateCommand = new DelegateCommand(OnCreate);
            _user = new UserModel();
            IsValidationSuccessful = false;
            _user.PropertyChanged += UserPropertyChanged;
        }

        private bool _isValidationSuccessful;
        public bool IsValidationSuccessful
        {
            get { return _isValidationSuccessful; }
            set { SetProperty(ref _isValidationSuccessful, value); }
        }

        private EventAggregator _eventAggregator;

        private UserModel _user;
        public UserModel User
        {
            get { return _user; }
            set { SetProperty(ref _user, value); }
        }

        public DelegateCommand CreateCommand { get; private set; }

        private void OnCreate()
        {
            FBAdmin.CreateUser(User);
            User.PropertyChanged -= UserPropertyChanged;
            User = new UserModel();
            User.PropertyChanged += UserPropertyChanged;
            IsValidationSuccessful = false;
        }

        void UserPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (User.Login != null &&
               User.Password != null &&
               User.DisplayName != null &&
               User.PhoneNumber != null &&
               User.Email != null)
            {
                IsValidationSuccessful = true;
            }
            else
            {
                IsValidationSuccessful = false;
            }
        }
    }
}
