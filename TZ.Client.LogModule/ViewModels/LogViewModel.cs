﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using Prism.Mvvm;
using Prism.Events;
using TZ.Client.LogModule.Model;
using TZ.Client.Base.Events;

namespace TZ.Client.LogModule.ViewModels
{
    class LogViewModel: BindableBase
    {
        public LogViewModel(EventAggregator _eventAggregator)
        {
            Logs = new ObservableCollection<LogModel>();
            eventAggregator = _eventAggregator;
            eventAggregator.GetEvent<LogMessegeEvent>().Subscribe(AddLogMessege);
        }

        public ObservableCollection<LogModel> Logs { get; set; }

        EventAggregator eventAggregator;

        private void AddLogMessege(string messege)
        {
            Logs.Add(new LogModel()
            {
                DateTime = DateTime.Now,
                Messege = messege,
            });
        }
    }
}
