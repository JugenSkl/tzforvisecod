﻿using Prism.Mvvm;
using Prism.Events;
using System.Collections.ObjectModel;
using TZ.Client.Base.Events;
using TZ.Client.FBAdminModule.Classes;
using TZ.Client.FBAdminModule.Models;
using Prism.Commands;

namespace TZ.Client.FBAdminModule.ViewModels
{
    class FBUsersViewModel: BindableBase
    {
        public FBUsersViewModel(EventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
            Users =  FBAdmin.GetUsers();
            _eventAggregator.GetEvent<CreateUserEvent>().Subscribe(AddUser);
            RemoveUserCommand = new DelegateCommand<UserModel>(OnRemoveUser);
        }

        private EventAggregator _eventAggregator;

        private UserModel _selectedUser;
        public UserModel SelectedUser
        {
            get { return _selectedUser; }
            set { SetProperty(ref _selectedUser, value); }
        }

        public ObservableCollection<UserModel> Users { get; set; }

        void AddUser(object value)
        {
            if(value is UserModel)
            {
                Users.Add(value as UserModel);
            }
        }

        public DelegateCommand<UserModel> RemoveUserCommand { get; private set; }

        private void OnRemoveUser(UserModel user)
        {
            FBAdmin.RemoveUser(user.Login);
            Users.Remove(user);
        }
    }
}
