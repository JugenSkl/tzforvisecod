﻿using FirebaseAdmin.Auth;
using Prism.Mvvm;

namespace TZ.Client.FBAdminModule.Models
{
    public class UserModel:BindableBase
    {
        public UserModel()
        {

        }

        public UserModel(UserRecord user)
        {
            Login = user.Uid;
            Email = user.Email;
            DisplayName = user.DisplayName;
            PhoneNumber = user.PhoneNumber;
            EmailVerified = user.EmailVerified;
        }

        private string _login;
        public string Login
        {
            get { return _login; }
            set { SetProperty(ref _login, value); }
        }

        private string _password;
        public string Password
        {
            get { return _password; }
            set { SetProperty(ref _password, value); }
        }

        private string _displayName;
        public string DisplayName
        {
            get { return _displayName; }
            set { SetProperty(ref _displayName, value); }
        }

        private string _phoneNumber;
        public string PhoneNumber
        {
            get { return _phoneNumber; }
            set { SetProperty(ref _phoneNumber, value); }
        }

        private string _email;
        public string Email
        {
            get { return _email; }
            set { SetProperty(ref _email, value); }
        }

        private bool _emailVerified;
        public bool EmailVerified
        {
            get { return _emailVerified; }
            set { SetProperty(ref _emailVerified, value); }
        }

    }
}
