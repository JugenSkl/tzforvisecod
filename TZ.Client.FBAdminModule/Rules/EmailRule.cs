﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Controls;
using TZ.Client.FBAdminModule.Classes;

namespace TZ.Client.FBAdminModule.Rules
{
    class EmailRule : ValidationRule
    {
        string pattern = @"\A[^@]+@([^@\.]+\.)+[^@\.]+\z";

        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            string email = value as string;
            if (Regex.IsMatch(email, pattern))
            {
                if (FBAdmin.GetUserByEmail(email) == null)
                {
                    return new ValidationResult(true, null);
                }
                else
                {
                    return new ValidationResult(false, "Email is used.");
                }
            }
            else
            {
                return new ValidationResult(false, "Incorrect Email.");
            }
        }
    }
}
