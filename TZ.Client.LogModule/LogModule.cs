﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prism.Ioc;
using Prism.Modularity;
using Prism.Regions;
using TZ.Client.Base;
using TZ.Client.LogModule.Views;

namespace TZ.Client.LogModule
{
    public class LogModule: IModule
    {
        public void OnInitialized(IContainerProvider containerProvider)
        {
            var regionManager = containerProvider.Resolve<IRegionManager>();
            regionManager.RegisterViewWithRegion(RegionNames.BottomRegion,typeof(LogView));
        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {

        }
    }
}
