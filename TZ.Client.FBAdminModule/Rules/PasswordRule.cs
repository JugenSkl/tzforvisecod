﻿
using System.Globalization;
using System.Windows.Controls;

namespace TZ.Client.FBAdminModule.Rules
{
    public class PasswordRule : ValidationRule
    {
        private const int _minLength = 5;
        private const int _maxLength = 20;

        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            string Value = value as string;
            if (Value.Length < _minLength || Value.Length > _maxLength)
            {
                return new ValidationResult(false, "The password length should be between 6 and 20 characters");
            }
            else
            {
                return new ValidationResult(true, null);
            }
        }
    }
}
