﻿using System;
using System.Globalization;
using System.Windows.Controls;
using TZ.Client.FBAdminModule.Classes;

namespace TZ.Client.FBAdminModule.Rules
{
    class LoginRule : ValidationRule
    {
        private bool _result = true;
        public bool Result
        {
            get { return _result; }
            set { _result = value; }
        }

        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {

            string login = value as string;
            if (FBAdmin.GetUser(login) == null)
            {
                Result = true;
                return new ValidationResult(Result, null);
            }
            else
            {
                Result = false;
                return new ValidationResult(Result, "A user with that name already exists.");
            }
        }
    }
}
