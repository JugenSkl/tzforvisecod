﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using FirebaseAdmin;
using FirebaseAdmin.Auth;
using Google.Apis.Auth.OAuth2;
using TZ.Client.FBAdminModule.Models;
using TZ.Client.Base.Events;
using Prism.Events;

namespace TZ.Client.FBAdminModule.Classes
{
    public static class FBAdmin
    {
        static FBAdmin()
        {
            _eventAggregator = CommonServiceLocator.ServiceLocator.Current.GetInstance<EventAggregator>();
            try
            {
                var Options = new AppOptions()
                {

                    Credential = GoogleCredential.FromFile("vcodtz-firebase-adminsdk-oy9h1-8d563d2564.json"),
                    ProjectId = "vcodtz",
                };
                var firebaseApp = FirebaseApp.Create(Options);
                _firebaseAuth = FirebaseAuth.GetAuth(firebaseApp);
            }
            catch (FirebaseException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private static EventAggregator _eventAggregator;

        private static FirebaseAuth _firebaseAuth;

        private static ObservableCollection<UserModel> _users;

        public static async void CreateUser(UserModel user)
        {
            try
            {
                UserRecordArgs args = new UserRecordArgs()
                {
                    Uid = user.Login,
                    DisplayName = user.DisplayName,
                    Password = user.Password,
                    Email = user.Email,
                    EmailVerified = false,
                    PhoneNumber = user.PhoneNumber
                };
                UserRecord _user = await _firebaseAuth.CreateUserAsync(args);
                _eventAggregator.GetEvent<CreateUserEvent>().Publish(new UserModel(_user));
                _eventAggregator.GetEvent<LogMessegeEvent>().Publish($"Successfully created new user: { user.Login }");
            }      
            catch (FirebaseException ex)
            {
                _eventAggregator.GetEvent<LogMessegeEvent>().Publish(ex.Message);
            }
        }

        public static ObservableCollection<UserModel> GetUsers()
        {
            _users = new ObservableCollection<UserModel>();
            GetUsersAsync();
            return _users;
        }

        private static async void GetUsersAsync()
        {
            try
            {
                var enumerator = _firebaseAuth.ListUsersAsync(null).GetEnumerator();
                while (await enumerator.MoveNext())
                {
                    UserRecord userFB = enumerator.Current;
                    var user = new UserModel(userFB);
                    _users.Add(user);
                }
                _eventAggregator.GetEvent<LogMessegeEvent>().Publish("The list of users has been successfully received.");
            }
            catch (FirebaseException ex)
            {
                _eventAggregator.GetEvent<LogMessegeEvent>().Publish(ex.Message);
            }
           
        }

        public static async void RemoveUser(string uid)
        {
            try
            {
                await _firebaseAuth.DeleteUserAsync(uid);
                _eventAggregator.GetEvent<LogMessegeEvent>().Publish($"Successfully deleted use: { uid }");
            }
            catch (FirebaseException ex)
            {
                _eventAggregator.GetEvent<LogMessegeEvent>().Publish(ex.Message);
            } 
        }

        public static UserRecord GetUser(string uid)
        {
            try
            {
                var temp = _firebaseAuth.GetUserAsync(uid);
                return temp.Result;
            }
            catch (System.Exception)
            {
                return null;
            }
        }

        public static UserRecord GetUserByEmail(string email)
        {
            try
            {
                var temp = _firebaseAuth.GetUserByEmailAsync(email);
                return temp.Result;
            }
            catch (System.Exception)
            {
                return null;
            }
            
        }
    }
}
