﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prism.Ioc;
using Prism.Modularity;
using Prism.Regions;
using TZ.Client.FBAdminModule.Views;
using TZ.Client.Base;

namespace TZ.Client.FBAdminModule
{
    public class FBAdminModule: IModule
    {
        public void OnInitialized(IContainerProvider containerProvider)
        {
            var regionManager = containerProvider.Resolve<IRegionManager>();
            regionManager.RegisterViewWithRegion(RegionNames.WorkingRegion, typeof(FBUsersView));
            regionManager.RegisterViewWithRegion(RegionNames.RightRegion, typeof(AddNewUserView));
        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {

        }
    }
}
